const mongoose = require("mongoose");
require("dotenv").config();

const url = process.env.TEST_MONGO_URI;

mongoose.connect(
  url,
  { useNewUrlParser: true, useUnifiedTopology: true },
  async err => {
    if (err) return console.log(err);
    const output = await removeAllCollections();
    console.log("ouput after removeAllCollections==========>", output);
    process.exit(0);
  }
);

async function removeAllCollections() {
  const collections = await mongoose.connection.db.collections();
  for (let collection of collections) {
    const output = await collection.remove();
    //   console.log('ouput for collection remove==========>', output);
  }
  return "All Okay!";
}
