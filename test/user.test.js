const request = require("supertest");
const app = require("../app");
const db = require("../models/db");

beforeAll(async () => {
  jest.setTimeout(10000);
});

describe("Test cases", () => {
  let userToRegister = {
    email: "priyanka@abc.com",
    name: "Priyanka",
    password: "1abcdefgh",
    confirmPassword: "1abcdefgh"
  };
  let loginCred = {
    email: "priyanka@abc.com",
    password: "1abcdefgh"
  };

  let verificationCode = "";

  describe("Testing index APIs", () => {
    it("should register a new user", async done => {
      await request(app)
        .post("/register")
        .send(userToRegister)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(201)
        .then(response => {
          expect(response.status).toBe(201);
          expect(response.body).toHaveProperty(
            "message",
            "User registration is successful"
          );
          expect(response.body).toHaveProperty("verificationCode");
          expect(response.body.verificationCode).not.toBeUndefined();
          verificationCode = response.body.verificationCode
            ? response.body.verificationCode
            : "";
        });
      done();
    });

    it("should verify the account", async done => {
      await request(app)
        .get("/accountverification/" + verificationCode)
        .set("Accept", "application/json")
        .expect(200)
        .then(response => {
          expect(response.status).toEqual(200);
          expect(response.body).toHaveProperty(
            "message",
            "Account verification is done successfully"
          );
        });
      done();
    });

    it("should login with credentials", async done => {
      await request(app)
        .post("/login")
        .send(loginCred)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .then(response => {
          expect(response.status).toEqual(200);
          expect(response.body).toHaveProperty("message", "Login Successful");
        });
      done();
    });
  });
});
