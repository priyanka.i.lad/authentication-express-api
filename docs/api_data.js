define({ "api": [
  {
    "type": "post",
    "url": "/accountverification/:code",
    "title": "Verify user account with verification code",
    "name": "Account_Verification",
    "group": "Index",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Password is reset successfully</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"Account verification is done successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Verification",
            "description": "<p>code is invalid or expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n   \"message\": \"Verification code is invalid or expired\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/index.js",
    "groupTitle": "Index"
  },
  {
    "type": "post",
    "url": "/forgotpassword",
    "title": "Request forgot password",
    "name": "Forgot_Password",
    "group": "Index",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "boolean",
            "optional": false,
            "field": "isVerified",
            "description": "<p>check  weather user's  account  is verified  or  not</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Password Reset code is sent</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"Password Reset code is sent\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-422",
            "description": "<p>One or more fields have incorrect data format</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-401",
            "description": "<p>User with given email address is either not found or not verified</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n   \"message\": \"One or more fields have incorrect data format\",\n   \"errors\": [ 'Email is required' ]\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n   \"message\": \"User with given email address is either not found or not verified\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/index.js",
    "groupTitle": "Index"
  },
  {
    "type": "post",
    "url": "/login",
    "title": "Request login",
    "name": "Login",
    "group": "Index",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password of the user</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Login Successful</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>name of the user, array of recipe id of likes, dislikes and favourites</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>access-token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"Login Successful\",\n  \"data\": {\n     username: \"name of the user\",\n     likes:\"array of recipe ids of liked recipes of the user\",\n     dislikes:\"array of recipe ids of disliked recipes of the user\",\n     favourites:\"array of recipe ids of favourites recipes of the user\",\n   },\n   \"token\":\"access token\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-422",
            "description": "<p>One or more fields have incorrect data format</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-401",
            "description": "<p>Invalid email or password</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n   \"message\": \"One or more fields have incorrect data format\",\n   \"errors\": [ 'Email is required', 'Password is required' ]\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n   \"message\": \"Invalid email or password\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/index.js",
    "groupTitle": "Index"
  },
  {
    "type": "post",
    "url": "/register",
    "title": "Register new User",
    "name": "Register",
    "group": "Index",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Display name  of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "confirmPassword",
            "description": "<p>password  to  confirm</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>User registration is successful</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"User registration is successful\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-422",
            "description": "<p>One or more fields have incorrect data format</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-409",
            "description": "<p>User with given email address already exists</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n   \"message\": \"One or more fields have incorrect data format\",\n   \"errors\": [ 'Email is required', 'Password is required' ]\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 409:",
          "content": "HTTP/1.1 409 Conflict\n{\n   \"message\": \"User with given email address already exists\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/index.js",
    "groupTitle": "Index"
  },
  {
    "type": "put",
    "url": "/resetpassword/:code",
    "title": "Request Reset Password with reset code",
    "name": "Reset_Password",
    "group": "Index",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>New  Password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "confirmPassword",
            "description": "<p>New Confirm Password</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Password is reset successfully</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"Password is reset successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-422",
            "description": "<p>One or more fields have incorrect data format / Reset code is invalid or expired</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n   \"message\": \"One or more fields have incorrect data format\",\n   \"errors\": [ 'Email is required' ]\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n   \"message\": \"Reset code is invalid or expired\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/index.js",
    "groupTitle": "Index"
  },
  {
    "type": "get",
    "url": "/users/dislikes",
    "title": "get disliked recipes of the user",
    "name": "Disliked_recipes",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer &lt;YOUR_ACCESS_TOKEN&gt;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "Header-Example:\n{\n  \"Authorization\": \"Bearer <YOUR_ACCESS_TOKEN>\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>user disliked recipes</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>array of recipe ids</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"data\": [\"123\",\"345\",\"234\"]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-401",
            "description": "<p>User is not authorized / Invalid access token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-422",
            "description": "<p>User not found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n   \"message\": \"User is not authorized\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n   \"message\": \"Invalid access token\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n   \"message\": \"User not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "Users"
  },
  {
    "type": "get",
    "url": "/users/favourites",
    "title": "get favourite recipes of the user",
    "name": "Favourite_recipes",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer &lt;YOUR_ACCESS_TOKEN&gt;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "Header-Example:\n{\n  \"Authorization\": \"Bearer <YOUR_ACCESS_TOKEN>\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>user favourite recipes</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>array of recipe ids</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"data\": [\"123\",\"345\",\"234\"]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-401",
            "description": "<p>User is not authorized / Invalid access token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-422",
            "description": "<p>User not found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n   \"message\": \"User is not authorized\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n   \"message\": \"Invalid access token\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n   \"message\": \"User not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "Users"
  },
  {
    "type": "get",
    "url": "/users/likes",
    "title": "Get liked recipes of the user",
    "name": "Liked_recipes",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer &lt;YOUR_ACCESS_TOKEN&gt;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "Header-Example:\n{\n  \"Authorization\": \"Bearer <YOUR_ACCESS_TOKEN>\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>user liked recipes</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>array of recipe ids</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"data\": [\"123\",\"345\",\"234\"]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-401",
            "description": "<p>User is not authorized / Invalid access token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-422",
            "description": "<p>User not found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n   \"message\": \"User is not authorized\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n   \"message\": \"Invalid access token\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n   \"message\": \"User not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "Users"
  },
  {
    "type": "get",
    "url": "/users/profile",
    "title": "View user profile",
    "name": "Profile",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer &lt;YOUR_ACCESS_TOKEN&gt;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "Header-Example:\n{\n  \"Authorization\": \"Bearer <YOUR_ACCESS_TOKEN>\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>User profile data</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"data\": {\n             \"name\": \"test\",\n                \"email\": \"test@abc.com\",\n                \"address\": \"Address1\",\n                \"mobile\": \"123456\",\n                \"gender\": \"female\",\n                \"education\": \"BE\" \n          }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-401",
            "description": "<p>User is not authorized / Invalid access token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-422",
            "description": "<p>User not found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n   \"message\": \"User is not authorized\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n   \"message\": \"Invalid access token\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n   \"message\": \"User not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "Users"
  },
  {
    "type": "put",
    "url": "/users/profileupdate",
    "title": "Save or Update user profile",
    "name": "Profile_update",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Display name of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Address of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>Mobile number of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "gender",
            "description": "<p>Gender of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "education",
            "description": "<p>Education of the user</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>application/json</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer &lt;YOUR_ACCESS_TOKEN&gt;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "Header-Example:\n{\n  \"Content-Type\": \"application/json\",\n  \"Authorization\": \"Bearer <YOUR_ACCESS_TOKEN>\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>User profile saved successfully</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"message\": \"User profile saved successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-422",
            "description": "<p>One or more fields have incorrect data format</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-401",
            "description": "<p>User is not authorized / Invalid access token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n   \"message\": \"One or more fields have incorrect data format\",\n   \"errors\": [ 'Email is required' ]\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n   \"message\": \"User is not authorized\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n   \"message\": \"Invalid access token\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "Users"
  },
  {
    "type": "patch",
    "url": "/users/togglefavourites",
    "title": "toggle favourite recipe of the user",
    "name": "Toggle_Favourite",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer &lt;YOUR_ACCESS_TOKEN&gt;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "Header-Example:\n{\n  \"Authorization\": \"Bearer <YOUR_ACCESS_TOKEN>\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>toggle favourite successful</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>array of favourite recipe ids</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"data\": [\"123\",\"345\",\"234\"]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-401",
            "description": "<p>User is not authorized / Invalid access token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-422",
            "description": "<p>User not found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n   \"message\": \"User is not authorized\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n   \"message\": \"Invalid access token\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n   \"message\": \"User not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "Users"
  },
  {
    "type": "patch",
    "url": "/users/toggledislikes",
    "title": "toggle disliked recipe of the user",
    "name": "Toggle_disliked",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer &lt;YOUR_ACCESS_TOKEN&gt;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "Header-Example:\n{\n  \"Authorization\": \"Bearer <YOUR_ACCESS_TOKEN>\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>toggle dislike recipe successful</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>array of liked recipe ids, array of disliked recipe ids</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"data\": {\n     likes:[\"123\",\"345\",\"234\"],\n     dislikes:[\"789\"]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-401",
            "description": "<p>User is not authorized / Invalid access token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-422",
            "description": "<p>User not found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n   \"message\": \"User is not authorized\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n   \"message\": \"Invalid access token\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n   \"message\": \"User not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "Users"
  },
  {
    "type": "patch",
    "url": "/users/togglelikes",
    "title": "toggle liked recipe of the user",
    "name": "Toggle_liked",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Bearer &lt;YOUR_ACCESS_TOKEN&gt;</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header-Example:",
          "content": "Header-Example:\n{\n  \"Authorization\": \"Bearer <YOUR_ACCESS_TOKEN>\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>toggle like recipe successful</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>array of liked recipe ids, array of disliked recipe ids</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"data\": {\n     likes:[\"123\",\"345\",\"234\"],\n     dislikes:[\"789\"]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-401",
            "description": "<p>User is not authorized / Invalid access token</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "statuscode-422",
            "description": "<p>User not found</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n   \"message\": \"User is not authorized\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 401:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n   \"message\": \"Invalid access token\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response 422:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n   \"message\": \"User not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "Users"
  }
] });
