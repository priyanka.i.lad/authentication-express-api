var fs = require("fs");
var path = require("path");
var randomstring = require("randomstring");
var user_data_path = path.join(__dirname + "./../model/users.json");
var jwt = require("jsonwebtoken");
var User = require("../models/User");
var sendMail = require("./../utils/sendMail");
require("dotenv").config();

module.exports = {
  loginController: function(req, res, next) {
    let { email, password } = req.body;
    User.findOne({ email: email, isVerified: true }, function(err, user) {
      if (err) return next(err);
      if (!user) {
        return res.status(401).json({
          message: "User is either not found or not verified",
          data: {}
        });
      }
      user.comparePasswords(req.body.password, function(err, isMatch) {
        if (err) return next(err);
        if (!isMatch) {
          return res.status(401).json({
            message: "Invalid email or password",
            data: {}
          });
        }
        return res.status(200).json({
          message: "Login Successful",
          data: {
            username: user.name,
            likes: user.likes ? user.likes : [],
            dislikes: user.dislikes ? user.dislikes : [],
            favourites: user.favourites ? user.favourites : []
          },
          token: jwt.sign({ id: user._id }, process.env.JWT_SECRET)
        });
      });
    });
  },

  registerController: function(req, res, next) {
    let { email, name, password } = req.body;
    User.findOne({ email }, (err, user) => {
      if (err) return next(err);
      if (user) {
        console.log("user already exists");
        return res.status(409).json({
          message: "User with given email address already exists"
        });
      }
      let randomCode = randomstring.generate({
        length: 8,
        charset: "alphanumeric",
        capitalization: "uppercase"
      });
      let verificationCode = `${randomCode}.${Date.now()}`;
      let additional_properties = {
        active: true,
        isVerified: false,
        resetCode: "",
        verificationCode: verificationCode
      };
      let propeties = { ...req.body, ...additional_properties };
      User.create(propeties, (err, result) => {
        if (err) return next(err);
        let verificationLink =
          (process.env.ENV === "production"
            ? process.env.UI_CLIENT_URL
            : process.env.DEV_UI_CLIENT_URL) +
          "/accountverification/" +
          verificationCode;
        sendMail(
          {
            from: process.env.SMTP_FROM_MAIL,
            to: req.body.email,
            subject: "Account Verification",
            html:
              "<p>Dear " +
              req.body.name +
              "</p><br/><br/><p>Please click below link to verify your account</p><br/><a href=" +
              verificationLink +
              " target='_blank'>Verification Link</a>"
          },
          function(err, info) {
            if (err) return next(err);
            res.status(201).json({
              verificationCode:
                process.env.NODE_ENV !== "production"
                  ? verificationCode
                  : undefined,
              message: "User registration is successful"
            });
            console.log("Mail sent: " + info);
          }
        );
      });
    });
  },
  forgotPasswordController: function(req, res, next) {
    let { email } = req.body;
    User.findOne({ email: email, isVerified: true }, (err, user) => {
      if (err) return next(err);
      if (!user) {
        return res.status(401).json({
          message: "User is either not found or not verified"
        });
      }
      let randomCode = randomstring.generate({
        length: 8,
        charset: "alphanumeric",
        capitalization: "uppercase"
      });
      let resetCode = randomCode + "." + user._id + "." + Date.now();

      User.updateOne(
        {
          email
        },
        {
          $set: {
            resetCode: resetCode
          }
        },
        (err, updatedUser) => {
          if (err) return next(err);
          res.status(200).json({
            message: "Password Reset code is sent"
          });
          let resetLink =
            (process.env.ENV === "production"
              ? process.env.UI_CLIENT_URL
              : process.env.DEV_UI_CLIENT_URL) +
            "/resetpassword/" +
            resetCode;

          sendMail(
            {
              from: process.env.SMTP_FROM_MAIL,
              to: req.body.email,
              subject: "Reset Password",
              html:
                "<p>Dear " +
                user.name +
                "</p><br/><br/><p>Please click below link to reset your password</p><br/><a href=" +
                resetLink +
                " target='_blank'>Password Reset Link</a>"
            },
            function(err, info) {
              if (err) return next(err);
              console.log("Mail sent: " + info);
            }
          );
        }
      );
    });
  },
  resetPasswordController: function(req, res, next) {
    let { code } = req.params;
    let [, id, date] = code.split(".");
    User.findOne(
      { resetCode: code, _id: id, isVerified: true },
      (err, user) => {
        if (err) return next(err);
        if (!user) {
          return res.status(422).json({
            message: "Reset code is invalid or expired"
          });
        }
        let hours_diff = Math.floor(
          (new Date().getTime() - new Date(Number(date)).getTime()) /
            (60 * 60 * 1000)
        );
        if (hours_diff > 24) {
          return res.status(422).json({
            message: "Reset code is invalid or expired"
          });
        }
        user.password = req.body.password;
        user.save((err, updatedUser) => {
          if (err) return next(err);
          return res.status(200).json({
            message: "Password is reset successfully"
          });
        });
      }
    );
  },
  accountVerificationController: function(req, res, next) {
    let { code } = req.params;
    let [, date] = code.split(".");
    console.log("code: " + code);
    User.findOne({ verificationCode: code }, (err, user) => {
      if (err) return next(err);

      if (!user) {
        return res.status(422).json({
          message: "Verification code is invalid or expired"
        });
      }
      let hours_diff = Math.floor(
        (new Date().getTime() - new Date(Number(date)).getTime()) /
          (60 * 60 * 1000)
      );
      if (hours_diff > 24) {
        return res.status(422).json({
          message: "Verification code is invalid or expired"
        });
      }
      User.updateOne(
        {
          _id: user._id
        },
        {
          $set: {
            isVerified: true,
            verificationCode: ""
          }
        },
        (err, updatedUser) => {
          console.log(err);
          if (err) return next(err);
          return res.status(200).json({
            message: "Account verification is done successfully"
          });
        }
      );
    });
  }
};
