var jwt = require("jsonwebtoken");
var fs = require("fs");
var path = require("path");
var User = require("../models/User");
var toggleLikedDisliked = require("./../utils/toggleLikedDisliked");
var user_data_path = path.join(__dirname + "./../model/users.json");
require("dotenv").config();

module.exports = {
  viewProfileController: function(req, res, next) {
    let { user } = res.locals;
    let {
      name = "",
      email,
      address = "",
      phone = "",
      gender = "",
      education = ""
    } = user;

    return res.status(200).json({
      data: { name, email, address, phone, gender, education }
    });
  },
  saveProfileController: function(req, res, next) {
    let { user } = res.locals;
    console.log("user in controller: " + user);
    let { name, address, phone, gender, education } = req.body;
    user.name = name;
    user.address = address;
    user.phone = phone;
    user.gender = gender;
    user.education = education;
    User.updateOne({ _id: res.locals.user_id }, user, (err, updatedUser) => {
      if (err) return next(err);
      return res.status(200).json({
        message: "User profile saved successfully"
      });
    });
  },
  likesController: function(req, res, next) {
    let { user } = res.locals;
    res.status(200).json({
      message: "user liked recipes",
      data: user.likes ? user.likes : []
    });
  },
  dislikesController: function(req, res, next) {
    let { user } = res.locals;
    res.status(200).json({
      message: "user disliked recipes",
      data: user.dislikes ? user.dislikes : []
    });
  },
  favouritesController: function(req, res, next) {
    let { user } = res.locals;
    res.status(200).json({
      message: "user favourite recipes",
      data: user.favourites ? user.favourites : []
    });
  },
  toggleLikeController: function(req, res, next) {
    let { user } = res.locals;
    let { recipe_id } = req.params;
    let { likes, dislikes } = user;

    toggleLikedDisliked(recipe_id, likes, dislikes);
    User.updateOne(
      { email: user.email, _id: res.locals.user_id },
      {
        $set: {
          likes: likes,
          dislikes: dislikes
        }
      },
      (err, result) => {
        if (err) return next(err);
        res.status(200).json({
          message: "toggle like recipe successful",
          data: {
            likes: likes,
            dislikes: dislikes
          }
        });
      }
    );
  },
  toggleDislikeController: function(req, res, next) {
    let { user } = res.locals;
    let { recipe_id } = req.params;
    let { likes, dislikes } = user;
    toggleLikedDisliked(recipe_id, dislikes, likes);
    User.updateOne(
      { email: user.email, _id: res.locals.user_id },
      {
        $set: {
          dislikes: dislikes,
          likes: likes
        }
      },
      (err, result) => {
        if (err) return next(err);
        res.status(200).json({
          message: "toggle dislike recipe successful",
          data: {
            likes: likes,
            dislikes: dislikes
          }
        });
      }
    );
  },
  toggleFavouriteController: function(req, res, next) {
    let { user } = res.locals;
    let { recipe_id } = req.params;
    let { favourites } = user;
    toggleLikedDisliked(recipe_id, favourites, null);
    User.updateOne(
      { email: user.email, _id: res.locals.user_id },
      {
        $set: {
          favourites: favourites
        }
      },
      (err, result) => {
        if (err) return next(err);
        res.status(200).json({
          message: "toggle favourite recipe successful",
          data: {
            favourites: favourites
          }
        });
      }
    );
  }
};
