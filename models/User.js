var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var bcrypt = require("bcrypt");

var userSchema = new Schema({
  name: String,
  email: String,
  password: String,
  phone: { type: String, default: "" },
  education: { type: String, default: "" },
  gender: { type: String, default: "" },
  address: String,
  isActive: { type: Boolean, default: true },
  isVerified: { type: Boolean, default: false },
  resetCode: String,
  verificationCode: String,
  likes: Array,
  dislikes: Array,
  favourites: Array
});

userSchema.pre("save", function(next) {
  let user = this;
  if (!user.password) return next();
  bcrypt.hash(user.password, 10, (err, encryptedPassword) => {
    if (err) return next(err);
    user.password = encryptedPassword;
    user.resetCode = "";
    next();
  });
});

userSchema.methods.comparePasswords = function(inputPassword, callback) {
  bcrypt.compare(inputPassword, this.password, function(err, isMatch) {
    return callback(err, isMatch);
  });
};

module.exports = mongoose.model("User", userSchema);
