var User = require("./User");
module.exports = function(req, res, next) {
  User.findOne(
    {
      _id: res.locals.user_id
    },
    (err, user) => {
      console.log("in user exists " + err);
      if (err) return next(err);
      console.log(user);
      if (!user) {
        return res.status(422).json({
          message: "User not found"
        });
      }
      res.locals.user = user;
      return next();
    }
  );
};
