const mongoose = require("mongoose");

function connect() {
  return new Promise((resolve, reject) => {
    let mongo_url =
      process.env.NODE_ENV === "production"
        ? process.env.MONGO_URI
        : process.env.TEST_MONGO_URI;
    mongoose.connect(
      mongo_url,
      { useNewUrlParser: true, useUnifiedTopology: true },
      async (err, result) => {
        console.log("connected to " + mongo_url);
        if (err) reject(err);
        else {
          //console.log("removing all collection");
          //await removeAllCollections();
          resolve();
        }
      }
    );
  });
}

function close() {
  return mongoose.disconnect();
}

module.exports = { connect, close };
