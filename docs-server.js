var express = require("express");
var path = require("path");
require("dotenv").config();
var app = express();
app.use(express.static(path.join(__dirname, "docs")));
app.set("view engine", "html");

app.get("*", function(req, res) {
  res.sendFile("/docs/index.html");
});

app.listen(process.env.DOCS_PORT, function() {
  `Docs server started at port ${process.env.DOCS_PORT}`;
});
