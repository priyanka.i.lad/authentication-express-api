var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var mongoose = require("mongoose");
var db = require("./models/db");
require("dotenv").config();

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");

var app = express();
db.connect().then(err => {
  err
    ? console.log("Error occured while connecting to  database: " + err)
    : console.log("database connected...");
});

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "docs")));
app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.log("Error Detail: " + err);
  res.status(500).json({
    message: "Something went wrong. Please contact administrator"
  });
}

module.exports = app;
