var express = require("express");
var router = express.Router();
let {
  loginController,
  registerController,
  forgotPasswordController,
  resetPasswordController,
  accountVerificationController
} = require("../controllers/index");
var { validateRequest, validationRules } = require("./../utils/utilities");
let {
  loginRules,
  registerRules,
  forgotPasswordRules,
  resetPasswordRules
} = validationRules;

/**
 * @api {post} /login Request login
 * @apiName Login
 * @apiGroup Index
 *
 * @apiParam {String} email Email of the user
 * @apiParam {String} password Password of the user
 *
 * @apiSuccess {String} message Login Successful
 * @apiSuccess {Object} data name of the user, array of recipe id of likes, dislikes and favourites
 * @apiSuccess {String} token access-token
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "message": "Login Successful",
 *       "data": {
 *          username: "name of the user",
 *          likes:"array of recipe ids of liked recipes of the user",
 *          dislikes:"array of recipe ids of disliked recipes of the user",
 *          favourites:"array of recipe ids of favourites recipes of the user",
 *        },
 *        "token":"access token"
 *     }
 * @apiError statuscode-422 One or more fields have incorrect data format
 * @apiError statuscode-401 Invalid email or password / User is either not found or not verified
 * @apiError statuscode-401 
 * @apiErrorExample Error-Response 422:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *        "message": "One or more fields have incorrect data format",
 *        "errors": [ 'Email is required', 'Password is required' ]
 *     }

 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "message": "Invalid email or password"
 *     }
 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "message": "User is either not found or not verified"
 *     }
 */
router.post("/login", validateRequest(loginRules), loginController);
/**
 * @api {post} /register Register new User
 * @apiName Register
 * @apiGroup Index
 *
 * @apiParam {String} email Email of the user
 * @apiParam {String} name  Display name  of the user
 * @apiParam {String} password Password of the user
 * @apiParam {String} confirmPassword password  to  confirm
 *
 * @apiSuccess {String} message User registration is successful
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "message": "User registration is successful"
 *     }
 * @apiError statuscode-422 One or more fields have incorrect data format
 * @apiError statuscode-409 User with given email address already exists
 *
 * @apiErrorExample Error-Response 422:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *        "message": "One or more fields have incorrect data format",
 *        "errors": [ 'Email is required', 'Password is required' ]
 *     }

 * @apiErrorExample Error-Response 409:
 *     HTTP/1.1 409 Conflict
 *     {
 *        "message": "User with given email address already exists"
 *     }
 */
router.post("/register", validateRequest(registerRules), registerController);
/**
 * @api {put} /forgotpassword Request forgot password 
 * @apiName Forgot  Password
 * @apiGroup Index
 *
 * @apiParam {String} email Email of the user
 * @apiParam {boolean} isVerified  check  weather user's  account  is verified  or  not  
 *
 * @apiSuccess {String} message Password Reset code is sent
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "message": "Password Reset code is sent"
 *     }
 * @apiError statuscode-422 One or more fields have incorrect data format
 * @apiError statuscode-401 User is either not found or not verified
 *
 * @apiErrorExample Error-Response 422:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *        "message": "One or more fields have incorrect data format",
 *        "errors": [ 'Email is required' ]
 *     }

 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "message": "User is either not found or not verified"
 *     }
 */
router.put(
  "/forgotpassword",
  validateRequest(forgotPasswordRules),
  forgotPasswordController
);
/**
 * @api {put} /resetpassword/:code Request Reset Password with reset code
 * @apiName Reset  Password
 * @apiGroup Index
 *
 * @apiParam {String} password New  Password
 * @apiParam {String} confirmPassword  New Confirm Password
 *
 * @apiSuccess {String} message Password is reset successfully
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "message": "Password is reset successfully"
 *     }
 * @apiError statuscode-422 One or more fields have incorrect data format / Reset code is invalid or expired
 *
 * @apiErrorExample Error-Response 422:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *        "message": "One or more fields have incorrect data format",
 *        "errors": [ 'Email is required' ]
 *     }

 * @apiErrorExample Error-Response 422:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *        "message": "Reset code is invalid or expired"
 *     }
 */
router.put(
  "/resetpassword/:code",
  validateRequest(resetPasswordRules),
  resetPasswordController
);
/**
 * @api {post} /accountverification/:code Verify user account with verification code
 * @apiName Account Verification
 * @apiGroup Index
 *
 * @apiSuccess {String} message Password is reset successfully
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "message": "Account verification is done successfully"
 *     }
 * @apiError Verification code is invalid or expired
 * @apiErrorExample Error-Response 422:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *        "message": "Verification code is invalid or expired"
 *     }
 */
router.get("/accountverification/:code", accountVerificationController);
module.exports = router;
