var express = require("express");
var router = express.Router();
var {
  viewProfileController,
  saveProfileController,
  likesController,
  dislikesController,
  favouritesController,
  toggleLikeController,
  toggleDislikeController,
  toggleFavouriteController
} = require("./../controllers/users");
var {
  validateRequest,
  validationRules,
  isUserAuthorized
} = require("./../utils/utilities");

var isUserExists = require("../models/isUserExists");
var { userProfileRules } = validationRules;
/**
 * @api {get} /users/profile View user profile
 * @apiName Profile
 * @apiGroup Users
 *
 * @apiHeader {String} Authorization Bearer <YOUR_ACCESS_TOKEN>
 * @apiHeaderExample 
 *     Header-Example:
 *     {
 *       "Authorization": "Bearer <YOUR_ACCESS_TOKEN>"
 *     }
 * @apiSuccess {Object} data User profile data
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "data": {
 *                  "name": "test",
                    "email": "test@abc.com",
                    "address": "Address1",
                    "mobile": "123456",
                    "gender": "female",
                    "education": "BE" 
 *               }
 *     }
 * @apiError statuscode-401 User is not authorized / Invalid access token
 * @apiError statuscode-422 User not found
 *
 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "message": "User is not authorized"
 *     }
 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "message": "Invalid access token"
 *     }
 * @apiErrorExample Error-Response 422:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *        "message": "User not found"
 *     }
 */
router.get("/profile", isUserAuthorized, isUserExists, viewProfileController);

/**
 * @api {put} /users/profileupdate Save or Update user profile
 * @apiName Profile update
 * @apiGroup Users
 *
 * @apiParam {String} email Email of the user
 * @apiParam {String} name Display name of the user
 * @apiParam {String} address Address of the user
 * @apiParam {String} mobile Mobile number of the user
 * @apiParam {String} gender Gender of the user
 * @apiParam {String} education Education of the user
 *
 * @apiHeader {String} Content-Type application/json
 * @apiHeader {String} Authorization Bearer <YOUR_ACCESS_TOKEN>
 * @apiHeaderExample 
 *     Header-Example:
 *     {
 *       "Content-Type": "application/json",
 *       "Authorization": "Bearer <YOUR_ACCESS_TOKEN>"
 *     }
 * @apiSuccess {String} message User profile saved successfully
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "message": "User profile saved successfully"
 *     }
 * @apiError statuscode-422 One or more fields have incorrect data format
 * @apiError statuscode-401 User is not authorized / Invalid access token
 *
 * @apiErrorExample Error-Response 422:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *        "message": "One or more fields have incorrect data format",
 *        "errors": [ 'Email is required' ]
 *     }

 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "message": "User is not authorized"
 *     }
 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "message": "Invalid access token"
 *     }
 */
router.put(
  "/profileupdate",
  validateRequest(userProfileRules),
  isUserAuthorized,
  isUserExists,
  saveProfileController
);

/**
 * @api {get} /users/likes Get liked recipes of the user
 * @apiName Liked recipes
 * @apiGroup Users
 *
 * @apiHeader {String} Authorization Bearer <YOUR_ACCESS_TOKEN>
 * @apiHeaderExample
 *     Header-Example:
 *     {
 *       "Authorization": "Bearer <YOUR_ACCESS_TOKEN>"
 *     }
 * @apiSuccess {String} message user liked recipes
 * @apiSuccess {Array} data array of recipe ids
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "data": ["123","345","234"]
 *     }
 * @apiError statuscode-401 User is not authorized / Invalid access token
 * @apiError statuscode-422 User not found
 *
 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "message": "User is not authorized"
 *     }
 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "message": "Invalid access token"
 *     }
 * @apiErrorExample Error-Response 422:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *        "message": "User not found"
 *     }
 */
router.get("/likes", isUserAuthorized, isUserExists, likesController);

/**
 * @api {get} /users/dislikes get disliked recipes of the user
 * @apiName Disliked recipes
 * @apiGroup Users
 *
 * @apiHeader {String} Authorization Bearer <YOUR_ACCESS_TOKEN>
 * @apiHeaderExample
 *     Header-Example:
 *     {
 *       "Authorization": "Bearer <YOUR_ACCESS_TOKEN>"
 *     }
 * @apiSuccess {String} message user disliked recipes
 * @apiSuccess {Array} data array of recipe ids
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "data": ["123","345","234"]
 *     }
 * @apiError statuscode-401 User is not authorized / Invalid access token
 * @apiError statuscode-422 User not found
 *
 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "message": "User is not authorized"
 *     }
 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "message": "Invalid access token"
 *     }
 * @apiErrorExample Error-Response 422:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *        "message": "User not found"
 *     }
 */
router.get("/dislikes", isUserAuthorized, isUserExists, dislikesController);

/**
 * @api {get} /users/favourites get favourite recipes of the user
 * @apiName Favourite recipes
 * @apiGroup Users
 *
 * @apiHeader {String} Authorization Bearer <YOUR_ACCESS_TOKEN>
 * @apiHeaderExample
 *     Header-Example:
 *     {
 *       "Authorization": "Bearer <YOUR_ACCESS_TOKEN>"
 *     }
 * @apiSuccess {String} message user favourite recipes
 * @apiSuccess {Array} data array of recipe ids
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "data": ["123","345","234"]
 *     }
 * @apiError statuscode-401 User is not authorized / Invalid access token
 * @apiError statuscode-422 User not found
 *
 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "message": "User is not authorized"
 *     }
 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "message": "Invalid access token"
 *     }
 * @apiErrorExample Error-Response 422:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *        "message": "User not found"
 *     }
 */
router.get("/favourites", isUserAuthorized, isUserExists, favouritesController);

/**
 * @api {patch} /users/togglelikes toggle liked recipe of the user
 * @apiName Toggle liked
 * @apiGroup Users
 *
 * @apiHeader {String} Authorization Bearer <YOUR_ACCESS_TOKEN>
 * @apiHeaderExample
 *     Header-Example:
 *     {
 *       "Authorization": "Bearer <YOUR_ACCESS_TOKEN>"
 *     }
 * @apiSuccess {String} message toggle like recipe successful
 * @apiSuccess {Array} data array of liked recipe ids, array of disliked recipe ids
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "data": {
 *          likes:["123","345","234"],
 *          dislikes:["789"]
 *     }
 * @apiError statuscode-401 User is not authorized / Invalid access token
 * @apiError statuscode-422 User not found
 *
 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "message": "User is not authorized"
 *     }
 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "message": "Invalid access token"
 *     }
 * @apiErrorExample Error-Response 422:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *        "message": "User not found"
 *     }
 */
router.patch(
  "/togglelike/:recipe_id",
  isUserAuthorized,
  isUserExists,
  toggleLikeController
);

/**
 * @api {patch} /users/toggledislikes toggle disliked recipe of the user
 * @apiName Toggle disliked
 * @apiGroup Users
 *
 * @apiHeader {String} Authorization Bearer <YOUR_ACCESS_TOKEN>
 * @apiHeaderExample
 *     Header-Example:
 *     {
 *       "Authorization": "Bearer <YOUR_ACCESS_TOKEN>"
 *     }
 * @apiSuccess {String} message toggle dislike recipe successful
 * @apiSuccess {Array} data array of liked recipe ids, array of disliked recipe ids
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "data": {
 *          likes:["123","345","234"],
 *          dislikes:["789"]
 *     }
 * @apiError statuscode-401 User is not authorized / Invalid access token
 * @apiError statuscode-422 User not found
 *
 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "message": "User is not authorized"
 *     }
 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "message": "Invalid access token"
 *     }
 * @apiErrorExample Error-Response 422:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *        "message": "User not found"
 *     }
 */
router.patch(
  "/toggledislike/:recipe_id",
  isUserAuthorized,
  isUserExists,
  toggleDislikeController
);

/**
 * @api {patch} /users/togglefavourites toggle favourite recipe of the user
 * @apiName Toggle Favourite
 * @apiGroup Users
 *
 * @apiHeader {String} Authorization Bearer <YOUR_ACCESS_TOKEN>
 * @apiHeaderExample
 *     Header-Example:
 *     {
 *       "Authorization": "Bearer <YOUR_ACCESS_TOKEN>"
 *     }
 * @apiSuccess {String} message toggle favourite successful
 * @apiSuccess {Array} data array of favourite recipe ids
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "data": ["123","345","234"]
 *     }
 * @apiError statuscode-401 User is not authorized / Invalid access token
 * @apiError statuscode-422 User not found
 *
 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "message": "User is not authorized"
 *     }
 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *        "message": "Invalid access token"
 *     }
 * @apiErrorExample Error-Response 422:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *        "message": "User not found"
 *     }
 */
router.patch(
  "/togglefavourites/:recipe_id",
  isUserAuthorized,
  isUserExists,
  toggleFavouriteController
);

module.exports = router;
