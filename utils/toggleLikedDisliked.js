module.exports = function(recipe_id, operateOn, adjustFrom) {
  console.log("toggleLikedDisliked called");

  if (!operateOn) {
    operateOn = [];
    operateOn.push(recipe_id);
    removeFrom(recipe_id, adjustFrom);
  } else {
    let recipeIndex = operateOn.findIndex(x => x === recipe_id);
    if (recipeIndex === -1) {
      operateOn.push(recipe_id);
      removeFrom(recipe_id, adjustFrom);
    } else operateOn.splice(recipeIndex, 1);
  }
};

function removeFrom(recipe_id, adjustFrom) {
  if (adjustFrom) {
    let index = adjustFrom.findIndex(x => x === recipe_id);
    if (index > -1) adjustFrom.splice(index, 1);
  }
}
