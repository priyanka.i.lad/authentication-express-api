module.exports = {
  loginRules: {
    email: "required|email",
    password: "required|min:8|max:15|regex:/^(?=.*[0-9])/"
  },
  registerRules: {
    email: "required|email",
    name: "required|alpha",
    password: "required|min:8|max:15|regex:/^(?=.*[0-9])/",
    confirmPassword: "same:password"
  },
  forgotPasswordRules: {
    email: "required|email"
    // client_url: "required|url"
  },
  resetPasswordRules: {
    password: "required|min:8|max:15|regex:/^(?=.*[0-9])/",
    confirmPassword: "same:password"
  },
  userProfileRules: {
    name: "required|alpha",
    email: "required|email",
    address: "required|alpha_num",
    phone: "required|numeric",
    gender: "required|alpha",
    education: "required|alpha"
  }
};
